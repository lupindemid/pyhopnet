﻿using System;
using System.Collections.Generic;
using ConsoleApplication1.Base;
using NUnit.Framework;

namespace ConsoleApplication1.Test
{
	[TestFixture]
	public class SimpleTest
	{
		[SetUp]
		public void Setup()
		{
			DummyTask.count = 0;
			DummyFailTask.count = 0;
		}

		[TestCase]
		public void SimplePlanTest()
		{
			var p = new Planner<GenericMethod.DummyState>(1);
			var result = p.DoPlanning(new GenericMethod.DummyState(), new List<Task<GenericMethod.DummyState>> {new DummyTask()});
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual("dummy", result[0].Name);
		}

		[TestCase]
		public void SimpleTwoActionPlanTest()
		{
			var p = new Planner<GenericMethod.DummyState>(1);
			var result = p.DoPlanning(new GenericMethod.DummyState(), new List<Task<GenericMethod.DummyState>> {DummyTask.Get, DummyTask.Get});
			Assert.AreEqual(2, result.Count);
			Assert.AreEqual("dummy0", result[0].Name);
			Assert.AreEqual("dummy1", result[1].Name);
		}

		[TestCase]
		public void ComplexTaskNotFoundTest()
		{
			var p = new Planner<GenericMethod.DummyState>(1);
			p.DoPlanning(new GenericMethod.DummyState(), new List<Task<GenericMethod.DummyState>> {new GenericMethod.DummyComplexTask()});
			Assert.AreEqual(1, p.Errors.Count);
		}

		[TestCase]
		public void AddMethodTest()
		{
			var p = new Planner<GenericMethod.DummyState>(1);
			p.AddMethod(DummyMethod.Get, "complexTask");
			Assert.AreEqual(1, p.GetTaskMethods("complexTask"));
			p.AddMethod(DummyMethod.Get, "complexTask");
			Assert.AreEqual(2, p.GetTaskMethods("complexTask"));
		}



		[TestCase]
		public void SimpleFailTaskTest()
		{
			var p = new Planner<GenericMethod.DummyState>(1);
			var result = p.DoPlanning(new GenericMethod.DummyState(), new List<Task<GenericMethod.DummyState>> { DummyFailTask.Get});
			Assert.IsNull(result);
		}
		[TestCase]
		public void ComplexTaskTwoMethodFoundTest()
		{
			//Assert.Fail("Should fail right now probably");
			var p = new Planner<GenericMethod.DummyState>(2);
			p.AddMethod(
				new GenericMethod(0, state => new List<Task<GenericMethod.DummyState>>() { DummyTask.Get, DummyTask.Get, DummyFailTask.Get }),
				"complexTask");
			p.AddMethod(
				new GenericMethod(0, state => new List<Task<GenericMethod.DummyState>>() { DummyTask.Get, DummyTask.Get, DummyTask.Get }),
				"complexTask");
			var result = p.DoPlanning(new GenericMethod.DummyState(), new List<Task<GenericMethod.DummyState>> { new GenericMethod.DummyComplexTask() });
			Assert.AreEqual(0, p.Errors.Count);
			Assert.AreEqual(3, result.Count);
		}

		[TestCase]
		public void ComplexTaskFoundTest()
		{
			var p = new Planner<GenericMethod.DummyState>(2);
			p.AddMethod(
				new GenericMethod(0, state => new List<Task<GenericMethod.DummyState>>() {DummyTask.Get, DummyTask.Get, DummyTask.Get}),
				"complexTask");
		var result =	p.DoPlanning(new GenericMethod.DummyState(), new List<Task<GenericMethod.DummyState>> {new GenericMethod.DummyComplexTask()});
			Assert.AreEqual(0, p.Errors.Count);
			Assert.AreEqual(3, result.Count);
		}
	}
}