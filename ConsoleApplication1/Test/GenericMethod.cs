﻿using System;
using System.Collections.Generic;
using ConsoleApplication1.Base;

namespace ConsoleApplication1.Test
{
	internal class GenericMethod : DummyMethod
	{
		private readonly Func<DummyState, List<Task<DummyState>>> generator;

		public GenericMethod(uint i, Func<DummyState, List<Task<DummyState>>> generator) : base(i)
		{
			this.generator = generator;
		}

		public override List<Task<DummyState>> Tasks(DummyState state)
		{
			return generator(state);
		}

		internal class DummyComplexTask : ComplexTask<DummyState>
		{
			public DummyComplexTask() : base("complexTask")
			{
			}
		}

		[Serializable]
		internal class DummyState
		{
			public override string ToString()
			{
				return "";
			}
		}
	}

	internal class DummyFailTask : SimpleTask<GenericMethod.DummyState>
	{
		internal static int count;

		public DummyFailTask(int i)
			: base("fail" + i)
		{
		}

		public DummyFailTask()
			: base("fail")
		{
		}

		public static DummyFailTask Get
		{
			get { return new DummyFailTask(count++); }
		}

		public override GenericMethod.DummyState Apply(GenericMethod.DummyState state)
		{
			return null;
		}
	}

	internal class DummyTask : SimpleTask<GenericMethod.DummyState>
	{
		internal static int count;

		public DummyTask(int i) : base("dummy" + i)
		{
		}

		public DummyTask() : base("dummy")
		{
		}

		public static DummyTask Get
		{
			get { return new DummyTask(count++); }
		}

		public override GenericMethod.DummyState Apply(GenericMethod.DummyState state)
		{
			return state;
		}
	}

	internal class DummyMethod : Method<GenericMethod.DummyState>
	{
		private static uint count;

		public DummyMethod(uint i) : base("dummy" + i)
		{
		}

		public static DummyMethod Get
		{
			get { return new DummyMethod(count++); }
		}

		public override List<Task<GenericMethod.DummyState>> Tasks(GenericMethod.DummyState state)
		{
			return new List<Task<GenericMethod.DummyState>> {DummyTask.Get};
		}
	}
}