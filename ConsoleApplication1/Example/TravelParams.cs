﻿namespace ConsoleApplication1.Example
{
	public class TravelParams
	{
		public TravelParams()
		{
		}

		public TravelParams(int location, int dest)
		{
			Location = location;
			Dest = dest;
		}

		public TravelParams(int location)
		{
			Location = location;
		}

		public int Location { get; private set; }
		public int Dest { get; private set; }

		public override string ToString()
		{
			return string.Format("[Loc:{0};Dest{1}]", Location, Dest);
		}
	}
}