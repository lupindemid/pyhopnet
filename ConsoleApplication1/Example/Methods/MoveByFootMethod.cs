﻿using System.Collections.Generic;
using ConsoleApplication1.Base;
using ConsoleApplication1.Example.Operators;

namespace ConsoleApplication1.Example.Methods
{
	public class MoveByFootMethod : Method<TaxiRideState>
	{
		private readonly int dest;
		private readonly int location;

		public MoveByFootMethod(TravelParams parms)
			: base("moveByFoot")
		{
			location = parms.Location;
			dest = parms.Dest;
		}

		public override List<Task<TaxiRideState>> Tasks(TaxiRideState state)
		{
			if (TravelHelper.GetDist(dest, location) <= 2)
			{
				return new List<Task<TaxiRideState>>
				       {
					       new WalkSimpleTask(dest, location)
				       };
			}
			return null;
		}
	}
}