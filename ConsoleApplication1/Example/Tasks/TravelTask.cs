﻿using ConsoleApplication1.Base;

namespace ConsoleApplication1.Example.Tasks
{
	public class TravelTask : ComplexTask<TaxiRideState>
	{
		public TravelTask(TravelParams parmeters) : base("travelTask")
		{
			Parmeters = parmeters;
		}

		public TravelParams Parmeters { get; private set; }
	}
}