﻿using System;

namespace ConsoleApplication1.Example
{
	internal class TravelHelper
	{
		public static double TaxiPrice(int location, int taxiDest)
		{
			return 1.5 + GetDist(location, taxiDest);
		}

		public static int GetDist(int location, int taxiDest)
		{
			return Math.Abs(location - taxiDest);
		}
	}
}