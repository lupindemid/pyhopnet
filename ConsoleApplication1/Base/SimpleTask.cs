﻿namespace ConsoleApplication1.Base
{
	public abstract class SimpleTask<TState> : Task<TState>
	{
		protected SimpleTask(string name) : base(name)
		{
		}

		public override bool IsOperator
		{
			get { return true; }
		}

		public abstract TState Apply(TState state);
	}
}