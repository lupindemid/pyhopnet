﻿namespace ConsoleApplication1.Base
{
	public abstract class Task<TState>
	{
		protected Task(string name)
		{
			Name = name;
		}

		public string Name { get; private set; }
		public abstract bool IsOperator { get; }

		public override string ToString()
		{
			return Name;
		}
	}
}